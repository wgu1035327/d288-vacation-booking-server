<<<<<<< README.md

## D288 Vacation Booking Server

## Link to website
http://vacationbooking.s3-website.us-east-2.amazonaws.com

## Contributions

**C.  Construct four new packages, one for each of the following: controllers, entities, dao, and services. The packages will need to be used for a checkout form and vacations packages list.**


Note: The packages should be on the same level of the hierarchy.


Note: Construct a package named config and copy the RestDataConfig.java provided in the laboratory environment to the package. Modify it so that the package and imports have the correct package and import addresses. Copy the application.properties file that is provided in the laboratory environment into your application properties resource file.

---

The 5 packages are created, but only the config packages is reflected in the gitlab repository.

The other 4 packages are not reflected on the gitlab repository, it is probably due to them being empty.

The application.properties file was also added.

**D.  Write code for the entities package that includes entity classes and the enum designed to match the UML diagram.**

RestDataConfig.java
```
updating package import 

// line 3
import wgu.pa.d288.entities.*;
```
Cart.java
```
package wgu.pa.d288.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "carts")
@Getter
@Setter
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cart_id")
    private Long id;

    @Column(name = "order_tracking_number")
    private String orderTrackingNumber;

    @Column(name = "package_price")
    private BigDecimal package_price;

    @Column(name = "party_size")
    private int party_size;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusType status;

    @Column(name = "create_date")
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")
    @UpdateTimestamp
    private Date last_update;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<CartItem> cartItem = new HashSet<>();

}
```
CartItem.java
```
package wgu.pa.d288.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "cart_items")
@Getter
@Setter
public class CartItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cart_item_id")
    private Long id;

    @Column(name = "create_date")
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")
    @UpdateTimestamp
    private Date last_update;

    @ManyToOne
    @JoinColumn(name = "vacation_id", referencedColumnName = "vacation_id")
    private Vacation vacation;

    @ManyToOne
    @JoinColumn(name= "cart_id")
    private Cart cart;

    @ManyToMany
    @JoinTable(
            name = "excursion_cartitem",
            joinColumns = @JoinColumn(name = "cart_item_id"),
            inverseJoinColumns = @JoinColumn(name = "excursion_id")
    )
    private Set<Excursion> excursions;
}
```
Country.java
```
package wgu.pa.d288.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "countries")
@Getter
@Setter
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    private Long id;

    @Column(name = "country")
    private String country_name;

    @Column(name = "create_date")
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")
    @UpdateTimestamp
    private Date last_update;

    @OneToMany(mappedBy = "country", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Division> divisions;
}
```
Customer.java
```
package wgu.pa.d288.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "customers")
@Getter
@Setter
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    private Long id;

    @Column(name = "customer_first_name")
    private String firstName;

    @Column(name = "customer_last_name")
    private String lastName;

    @Column(name = "address")
    private String address;

    @Column(name = "postal_code")
    private String postal_code;

    @Column(name = "phone")
    private String phone;

    @Column(name = "create_date")
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")
    @UpdateTimestamp
    private Date last_update;

    @ManyToOne
    @JoinColumn(name = "division_id", referencedColumnName = "division_id")
    private Division division;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Cart> carts;

}
```
Division.java
```
package wgu.pa.d288.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;

@Entity
@Table(name = "divisions")
@Getter
@Setter
public class Division {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "division_id")
    private Long id;

    @Column(name = "division")
    private String division_name;

    @Column(name = "create_date")
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")
    @UpdateTimestamp
    private Date last_update;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id", nullable = false, insertable = false, updatable = false)
    private Country country;

    @Column(name = "country_id")
    private long country_id;

    public void setCountry(Country country) {
        setCountry_id(country.getId());
        this.country = country;
    }
}
```
Excursion.java
```
package wgu.pa.d288.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "excursions")
@Getter
@Setter
public class Excursion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "excursion_id")
    private Long id;

    @Column(name = "excursion_title")
    private String excursion_title;

    @Column(name = "excursion_price")
    private BigDecimal excursion_price;

    @Column(name = "image_url")
    private String image_URL;

    @Column(name = "create_date")
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")
    @UpdateTimestamp
    private Date last_update;

    @ManyToOne
    @JoinColumn(name = "vacation_id")
    private Vacation vacation;

    @ManyToMany(mappedBy = "excursions")
    private Set<CartItem> cartitems;
}
```
StatusType.java
```
package wgu.pa.d288.entities;

public enum StatusType {
    pending,
    ordered,
    canceled
}
```
Vacation.java
```
package wgu.pa.d288.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "vacations")
@Getter
@Setter
public class Vacation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vacation_id")
    private Long id;

    @Column(name = "vacation_title")
    private String vacation_title;

    @Column(name = "description")
    private String description;

    @Column(name = "travel_fare_price")
    private BigDecimal travel_price;

    @Column(name = "image_url")
    private String image_URL;

    @Column(name = "create_date")
    @CreationTimestamp
    private Date create_date;

    @Column(name = "last_update")
    @UpdateTimestamp
    private Date last_update;

    @OneToMany(mappedBy = "vacation", cascade = CascadeType.ALL)
    private Set<Excursion> excursions;

}
```
**E.  Write code for the dao package that includes repository interfaces for the entities that extend JpaRepository, and add cross-origin support.**

CartItemRepository.java
```
package wgu.pa.d288.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import wgu.pa.d288.entities.CartItem;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
public interface CartItemRepository extends JpaRepository<CartItem, Long> {
}
```
CartRepository.java
```
package wgu.pa.d288.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import wgu.pa.d288.entities.Cart;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
public interface CartRepository extends JpaRepository<Cart, Long> {
}
```
CountryRepository.java
```
package wgu.pa.d288.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import wgu.pa.d288.entities.Country;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
public interface CountryRepository extends JpaRepository<Country, Long> {
}

```
CustomerRepository.java
```
package wgu.pa.d288.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import wgu.pa.d288.entities.Customer;


@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}

```
DivisionRepository.java
```
package wgu.pa.d288.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import wgu.pa.d288.entities.Division;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
public interface DivisionRepository extends JpaRepository<Division, Long> {
}
```
ExcursionRepository.java
```
package wgu.pa.d288.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import wgu.pa.d288.entities.Excursion;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
public interface ExcursionRepository extends JpaRepository<Excursion, Long> {
}
```
VacationRepository.java
```
package wgu.pa.d288.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import wgu.pa.d288.entities.Vacation;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
public interface VacationRepository extends JpaRepository<Vacation, Long> {
}
```
**F.  Write code for the services package that includes each of the following:**

**•   a purchase data class with a customer cart and a set of cart items**

Purchase.java
```
package wgu.pa.d288.services;

import lombok.Data;
import wgu.pa.d288.entities.Cart;
import wgu.pa.d288.entities.CartItem;

import java.util.Set;

@Data
public class Purchase {

    private Cart cart;
    private Set<CartItem> cartItems;

}
```
**•   a purchase response data class that contains an order tracking number**

PurchaseResponse.java
```
package wgu.pa.d288.services;

import lombok.Data;

@Data
public class PurchaseResponse {

    private final String orderTrackingNumber;
}
```
**•   a checkout service interface**

CheckoutService.java
```
package wgu.pa.d288.services;

public interface CheckoutService {

    PurchaseResponse placeOrder(Purchase purchase);

}
```
**•   a checkout service implementation class**

CheckoutServiceImpl.java
```
package wgu.pa.d288.services;

import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;
import wgu.pa.d288.dao.CartRepository;
import wgu.pa.d288.entities.Cart;
import wgu.pa.d288.entities.CartItem;
import wgu.pa.d288.entities.StatusType;

import java.util.Set;
import java.util.UUID;

@Service
public class CheckoutServiceImpl implements CheckoutService {

    private CartRepository cartRepository;

    public CheckoutServiceImpl(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Override
    @Transactional
    public PurchaseResponse placeOrder(Purchase purchase) {

        // retrieve data for cart and cartItems
        Cart cart = purchase.getCart();
        Set<CartItem> cartItems = purchase.getCartItems();

        // associate cart with cartItems
        cartItems.forEach(item -> cart.add(item));

        // generate order tracking number and set cart.orderTrackingNumber
        String orderTrackingNumber = generateOrderTrackingNumber();
        cart.setOrderTrackingNumber(orderTrackingNumber);

        // set cart.status to "ordered"
        cart.setStatus(StatusType.ordered);

        // save cart to the database
        cartRepository.save(cart);

        return new PurchaseResponse(orderTrackingNumber);
    }

    private String generateOrderTrackingNumber() {
        return UUID.randomUUID().toString();
    }
}
```
Cart.java
```
implement the add() method for Cart objects

// line 55-65

    public void add(CartItem item) {

        if (item != null) {
            if (cartItem == null) {
                cartItem = new HashSet<>();
            }
            cartItem.add(item);
            System.out.println("Cart being added: " + this);
            item.setCart(this);
        }
    }
```
**G.  Write code to include validation to enforce the inputs needed by the Angular front-end.**

Customer.java
```
adding 'nullable=false' to the @Column of the fields for user input in Customer entity
// line 24-37
    @Column(name = "customer_first_name", nullable = false)
    private String firstName;

    @Column(name = "customer_last_name", nullable = false)
    private String lastName;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "postal_code", nullable = false)
    private String postal_code;

    @Column(name = "phone", nullable = false)
    private String phone;

// line 47-49
    @ManyToOne
    @JoinColumn(name = "division_id", referencedColumnName = "division_id", nullable = false)
    private Division division;
```
application.properties
```
// line 6
spring.jpa.properties.hibernate.check_nullability=true
```
CheckoutServiceImpl.java
```
// line 30-33
    // Display an error message to the Order confirmation page
    if (cartItems.isEmpty()) {
        return new PurchaseResponse("CART IS EMPTY, PLEASE ADD ITEMS. NOTHING HAS BEEN SAVED TO DATABASE.");
    }
```
**H.  Write code for the controllers package that includes a REST controller checkout controller class with a post mapping to place orders.**

CheckoutController.java
```
package wgu.pa.d288.controllers;

import org.springframework.web.bind.annotation.*;
import wgu.pa.d288.services.CheckoutService;
import wgu.pa.d288.services.Purchase;
import wgu.pa.d288.services.PurchaseResponse;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
@RestController
@RequestMapping("/api/checkout")
public class CheckoutController {
    
    private CheckoutService checkoutService;

    public CheckoutController(CheckoutService checkoutService) {
        this.checkoutService = checkoutService;
    }

    @PostMapping("/purchase")
    public PurchaseResponse placeOrder(@RequestBody Purchase purchase) {
        PurchaseResponse purchaseResponse = checkoutService.placeOrder(purchase);
        return purchaseResponse;
    }
}
```
**I.  Add five sample customers to the application programmatically.**
BootStrapData.java
```
package wgu.pa.d288.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import wgu.pa.d288.dao.CustomerRepository;
import wgu.pa.d288.dao.DivisionRepository;
import wgu.pa.d288.entities.Customer;

@Component
public class BootStrapData implements CommandLineRunner {

    private final CustomerRepository customerRepository;

    private final DivisionRepository divisionRepository;

    public BootStrapData(DivisionRepository divisionRepository, CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
        this.divisionRepository = divisionRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        
        // only add sample data when there is only 1 entry in the database, in accordance with the given SQL script
        if(customerRepository.count() == 1) {
            Customer customer = new Customer("Jack", "Pham", "1112 Somewhere", "12345", "(111) 324-2345", divisionRepository.findById(31L).get());
            Customer customer2 = new Customer("Jill", "Dee", "222 Righthere", "22222", "(222) 222-3345", divisionRepository.findById(32L).get());
            Customer customer3 = new Customer("Dente", "Jako", "33345 Overthere", "33333", "(333) 333-4455", divisionRepository.findById(74L).get());
            Customer customer4 = new Customer("Ali", "Kola", "44456 Corner", "44444", "(444) 444-5566", divisionRepository.findById(47L).get());
            Customer customer5 = new Customer("Loki", "Asga", "555 Someplace", "55555", "(555) 555-7788", divisionRepository.findById(75L).get());
            customerRepository.save(customer);
            customerRepository.save(customer2);
            customerRepository.save(customer3);
            customerRepository.save(customer4);
            customerRepository.save(customer5);
        }
    }
}
```
Customer.java
```
add constructor to facilitate adding new customers in BootStrapData.java

// line 54-62
    public Customer() {}
    public Customer(String firstName, String lastName, String address, String postal_code, String phone, Division division) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.postal_code = postal_code;
        this.phone = phone;
        this.division = division;
    }
```