package wgu.pa.d288.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import wgu.pa.d288.dao.*;
import wgu.pa.d288.entities.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class BootStrapData implements CommandLineRunner {

    private final CustomerRepository customerRepository;
    private final DivisionRepository divisionRepository;
    private final CountryRepository countryRepository;
    private final VacationRepository vacationRepository;
    private final ExcursionRepository excursionRepository;

    public BootStrapData(DivisionRepository divisionRepository, CustomerRepository customerRepository, CountryRepository countryRepository, VacationRepository vacationRepository, ExcursionRepository excursionRepository) {
        this.customerRepository = customerRepository;
        this.divisionRepository = divisionRepository;
        this.countryRepository = countryRepository;
        this.vacationRepository = vacationRepository;
        this.excursionRepository = excursionRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        if(countryRepository.findAll().isEmpty()) {
            countryRepository.saveAll(new ArrayList<>(Arrays.asList(
                    new Country(1L, "U.S"),
                    new Country(2L, "UK"),
                    new Country(3L, "Canada")
            )));
        }

        if(divisionRepository.findAll().isEmpty() ) {
            divisionRepository.saveAll(new ArrayList<>(Arrays.asList(
                    new Division("Alaska", 1),
                    new Division("Arizona", 1 ),
                    new Division("Arkansas", 1 ),
                    new Division("California", 1 ),
                    new Division("Colorado", 1 ),
                    new Division("Connecticut", 1 ),
                    new Division("Delaware", 1 ),
                    new Division("District of Columbia", 1 ),
                    new Division("Florida", 1 ),
                    new Division("Georgia", 1 ),
                    new Division("Hawaii", 1 ),
                    new Division("Idaho", 1 ),
                    new Division("Illinois", 1 ),
                    new Division("Indiana", 1 ),
                    new Division("Iowa", 1 ),
                    new Division("Kansas", 1 ),
                    new Division("Kentucky", 1 ),
                    new Division("Louisiana", 1 ),
                    new Division("Maine", 1 ),
                    new Division("Maryland", 1 ),
                    new Division("Massachusetts", 1 ),
                    new Division("Michigan", 1 ),
                    new Division("Minnesota", 1 ),
                    new Division("Mississippi", 1 ),
                    new Division("Missouri", 1 ),
                    new Division("Montana", 1 ),
                    new Division("Nebraska", 1 ),
                    new Division("Nevada", 1 ),
                    new Division("New Hampshire", 1 ),
                    new Division("New Jersey", 1 ),
                    new Division("New Mexico", 1 ),
                    new Division("New York", 1 ),
                    new Division("North Carolina", 1 ),
                    new Division("North Dakota", 1 ),
                    new Division("Ohio", 1 ),
                    new Division("Oklahoma", 1 ),
                    new Division("Oregon", 1 ),
                    new Division("Pennsylvania", 1 ),
                    new Division("Rhode Island", 1 ),
                    new Division("South Carolina", 1 ),
                    new Division("South Dakota", 1 ),
                    new Division("Tennessee", 1 ),
                    new Division("Texas", 1 ),
                    new Division("Utah", 1 ),
                    new Division("Vermont", 1 ),
                    new Division("Virginia", 1 ),
                    new Division("Washington", 1 ),
                    new Division("West Virginia", 1 ),
                    new Division("Wisconsin", 1 ),
                    new Division("Wyoming", 1 ),
                    new Division("Alberta", 3 ),
                    new Division("British Columbia", 3 ),
                    new Division("Manitoba", 3 ),
                    new Division("New Brunswick", 3 ),
                    new Division("Newfoundland and Labrador", 3 ),
                    new Division("Northwest Territories", 3 ),
                    new Division("Nova Scotia", 3 ),
                    new Division("Nunavut", 3 ),
                    new Division("Ontario", 3 ),
                    new Division("Prince Edward Island", 3 ),
                    new Division("Québec", 3 ),
                    new Division("Saskatchewan", 3 ),
                    new Division("Yukon", 3 ),
                    new Division("England", 2 ),
                    new Division("Wales", 2 ),
                    new Division("Scotland", 2 ),
                    new Division("Northern Ireland", 2 )
            )));
        }

        if(customerRepository.findAll().isEmpty()) {
            Division division1 = divisionRepository.findByName("North Carolina").get();
            Division division2 = divisionRepository.findByName("Wisconsin").get();
            Division division3 = divisionRepository.findByName("Nova Scotia").get();
            Division division4 = divisionRepository.findByName("Wales").get();
            Division division5 = divisionRepository.findByName("Northern Ireland").get();

            Customer customer = new Customer("Jack", "Pham", "1112 Somewhere", "12345", "(111) 324-2345", division1);
            Customer customer2 = new Customer("Jill", "Dee", "222 Righthere", "22222", "(222) 222-3345", division2);
            Customer customer3 = new Customer("Dente", "Jako", "33345 Overthere", "33333", "(333) 333-4455", division3);
            Customer customer4 = new Customer("Ali", "Kola", "44456 Corner", "44444", "(444) 444-5566", division4);
            Customer customer5 = new Customer("Loki", "Asga", "555 Someplace", "55555", "(555) 555-7788", division5);
            customerRepository.saveAll(new ArrayList<>(Arrays.asList(
                    customer, customer2, customer4, customer5
            )));
        }

        if(vacationRepository.findAll().isEmpty()) {
            vacationRepository.saveAll(new ArrayList<>(Arrays.asList(
                    new Vacation("Visit the beautiful country of Italy",
                            "https://images.unsplash.com/photo-1515859005217-8a1f08870f59?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1110&q=80",
                            new BigDecimal("1000"),
                            "Italy"),
                    new Vacation("Visit the beautiful country of Greece", "https://images.unsplash.com/photo-1533105079780-92b9be482077?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                            new BigDecimal("1500"),
                            "Greece"),
                    new Vacation("Visit the beautiful country of France",
                    "https://images.unsplash.com/photo-1502602898657-3e91760cbb34?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1473&q=80",
                            new BigDecimal("1500"),
                            "France"),
                    new Vacation("Visit the beautiful country of Belgium",
                    "https://images.unsplash.com/photo-1491557345352-5929e343eb89?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            new BigDecimal("1500"),
                            "Belgium"),
                    new Vacation("Visit the beautiful country of Brazil",
                    "https://images.unsplash.com/photo-1483729558449-99ef09a8c325?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            new BigDecimal("1500"),
                            "Brazil"),
                    new Vacation("Visit the beautiful state of South Dakota",
                    "https://images.unsplash.com/photo-1605801495512-f47a64d01f4f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1472&q=80",
                            new BigDecimal("1500"),
                            "South Dakota"),
                    new Vacation("Visit the beautiful city of Nashville",
                    "https://images.unsplash.com/photo-1545419913-775e3e82c7db?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1636&q=80",
                            new BigDecimal("1500"),
                            "Nashville"),
                    new Vacation("Visit the beautiful state of Wisconsin",
                    "https://images.unsplash.com/photo-1566419808810-658178380987?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1471&q=80",
                            new BigDecimal("1500"),
                            "Wisconsin")
            )));
        }

        if (excursionRepository.findAll().isEmpty() ) {
            Vacation Italy = vacationRepository.getVacationByVacation_title("Italy");
            Vacation Greece = vacationRepository.getVacationByVacation_title("Greece");
            Vacation France = vacationRepository.getVacationByVacation_title("France");
            Vacation Belgium = vacationRepository.getVacationByVacation_title("Belgium");
            Vacation Brazil = vacationRepository.getVacationByVacation_title("Brazil");
            Vacation Nashville = vacationRepository.getVacationByVacation_title("Nashville");
            Vacation Wisconsin = vacationRepository.getVacationByVacation_title("Wisconsin");
            Vacation South_Dakota = vacationRepository.getVacationByVacation_title("South Dakota");

            excursionRepository.saveAll(new ArrayList<>(Arrays.asList(
                    new Excursion(new BigDecimal("100"), "Cheese Tour",
                            "https://images.unsplash.com/photo-1631379578550-7038263db699?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1474&q=80",
                            Italy
                    ),
                    new Excursion(
                            new BigDecimal("75"),
                            "Bicycle Tour",
                            "https://images.unsplash.com/uploads/14122621859313b34d52b/37e28531?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1473&q=80",
                            Italy
                    ),
                    new Excursion(new BigDecimal("250"), "Spa Treatment",
                            "https://images.unsplash.com/photo-1620733723572-11c53f73a416?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                            Italy
                    ), new Excursion(new BigDecimal("100"), "Historic Tour",
                            "https://images.unsplash.com/photo-1479142506502-19b3a3b7ff33?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Italy
                    ), new Excursion(new BigDecimal("25"), "Boat Ride",
                            "https://images.unsplash.com/photo-1587252337395-d02401a8a814?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1026&q=80",
                            Italy
                    ), new Excursion(new BigDecimal("500"), "Horseback Riding Lesson",
                            "https://images.unsplash.com/photo-1598810577851-34982c359155?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Italy
                    ), new Excursion(new BigDecimal("120"), "Zip Lining",
                            "https://images.unsplash.com/photo-1625076307714-a5cd1b2beb4f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Italy

                    ), new Excursion(new BigDecimal("150"), "Dinner and a Show",
                            "https://plus.unsplash.com/premium_photo-1661774645265-ce387923cb5b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Italy

                    ), new Excursion(new BigDecimal("100"), "Cheese Tour",
                            "https://images.unsplash.com/photo-1631379578550-7038263db699?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1474&q=80",
                            Greece

                    ), new Excursion(new BigDecimal("75"), "Bicycle Tour",
                            "https://images.unsplash.com/uploads/14122621859313b34d52b/37e28531?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1473&q=80",
                            Greece

                    ), new Excursion(new BigDecimal("250"), "Spa Treatment",
                            "https://images.unsplash.com/photo-1620733723572-11c53f73a416?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                            Greece

                    ), new Excursion(new BigDecimal("100"), "Historic Tour",
                            "https://images.unsplash.com/photo-1479142506502-19b3a3b7ff33?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Greece
                    ), new Excursion(new BigDecimal("25"), "Boat Ride",
                            "https://images.unsplash.com/photo-1587252337395-d02401a8a814?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1026&q=80",
                            Greece

                    ), new Excursion(new BigDecimal("500"), "Horseback Riding Lesson",
                            "https://images.unsplash.com/photo-1598810577851-34982c359155?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Greece

                    ), new Excursion(new BigDecimal("120"), "Zip Lining",
                            "https://images.unsplash.com/photo-1625076307714-a5cd1b2beb4f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Greece

                    ), new Excursion(new BigDecimal("150"), "Dinner and a Show",
                            "https://plus.unsplash.com/premium_photo-1661774645265-ce387923cb5b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Greece

                    ), new Excursion(new BigDecimal("100"), "Cheese Tour",
                            "https://images.unsplash.com/photo-1631379578550-7038263db699?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1474&q=80",
                            France

                    ), new Excursion(new BigDecimal("75"), "Bicycle Tour",
                            "https://images.unsplash.com/uploads/14122621859313b34d52b/37e28531?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1473&q=80",
                            France

                    ), new Excursion(new BigDecimal("250"), "Spa Treatment",
                            "https://images.unsplash.com/photo-1620733723572-11c53f73a416?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                            France

                    ), new Excursion(new BigDecimal("100"), "Historic Tour",
                            "https://images.unsplash.com/photo-1479142506502-19b3a3b7ff33?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            France

                    ), new Excursion(new BigDecimal("25"), "Boat Ride",
                            "https://images.unsplash.com/photo-1587252337395-d02401a8a814?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1026&q=80",
                            France

                    ), new Excursion(new BigDecimal("500"), "Horseback Riding Lesson",
                            "https://images.unsplash.com/photo-1598810577851-34982c359155?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            France

                    ), new Excursion(new BigDecimal("120"), "Zip Lining",
                            "https://images.unsplash.com/photo-1625076307714-a5cd1b2beb4f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            France

                    ), new Excursion(new BigDecimal("150"), "Dinner and a Show",
                            "https://plus.unsplash.com/premium_photo-1661774645265-ce387923cb5b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            France

                    ), new Excursion(new BigDecimal("100"), "Cheese Tour",
                            "https://images.unsplash.com/photo-1631379578550-7038263db699?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1474&q=80",
                            Belgium

                    ), new Excursion(new BigDecimal("75"), "Bicycle Tour",
                            "https://images.unsplash.com/uploads/14122621859313b34d52b/37e28531?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1473&q=80",
                            Belgium

                    ), new Excursion(new BigDecimal("250"), "Spa Treatment",
                            "https://images.unsplash.com/photo-1620733723572-11c53f73a416?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                            Belgium

                    ), new Excursion(new BigDecimal("100"), "Historic Tour",
                            "https://images.unsplash.com/photo-1479142506502-19b3a3b7ff33?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Belgium

                    ), new Excursion(new BigDecimal("25"), "Boat Ride",
                            "https://images.unsplash.com/photo-1587252337395-d02401a8a814?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1026&q=80",
                            Belgium

                    ), new Excursion(new BigDecimal("500"), "Horseback Riding Lesson",
                            "https://images.unsplash.com/photo-1598810577851-34982c359155?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Belgium

                    ), new Excursion(new BigDecimal("120"), "Zip Lining",
                            "https://images.unsplash.com/photo-1625076307714-a5cd1b2beb4f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Belgium

                    ), new Excursion(new BigDecimal("150"), "Dinner and a Show",
                            "https://plus.unsplash.com/premium_photo-1661774645265-ce387923cb5b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Belgium

                    ), new Excursion(new BigDecimal("100"), "Cheese Tour",
                            "https://images.unsplash.com/photo-1631379578550-7038263db699?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1474&q=80",
                            Brazil

                    ), new Excursion(new BigDecimal("75"), "Bicycle Tour",
                            "https://images.unsplash.com/uploads/14122621859313b34d52b/37e28531?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1473&q=80",
                            Brazil

                    ), new Excursion(new BigDecimal("250"), "Spa Treatment",
                            "https://images.unsplash.com/photo-1620733723572-11c53f73a416?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                            Brazil

                    ), new Excursion(new BigDecimal("100"), "Historic Tour",
                            "https://images.unsplash.com/photo-1479142506502-19b3a3b7ff33?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Brazil

                    ), new Excursion(new BigDecimal("25"), "Boat Ride",
                            "https://images.unsplash.com/photo-1587252337395-d02401a8a814?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1026&q=80",
                            Brazil

                    ), new Excursion(new BigDecimal("500"), "Horseback Riding Lesson",
                            "https://images.unsplash.com/photo-1598810577851-34982c359155?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Brazil

                    ), new Excursion(new BigDecimal("120"), "Zip Lining",
                            "https://images.unsplash.com/photo-1625076307714-a5cd1b2beb4f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Brazil

                    ), new Excursion(new BigDecimal("150"), "Dinner and a Show",
                            "https://plus.unsplash.com/premium_photo-1661774645265-ce387923cb5b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Brazil

                    ), new Excursion(new BigDecimal("100"), "Cheese Tour",
                            "https://images.unsplash.com/photo-1631379578550-7038263db699?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1474&q=80",
                            South_Dakota
                    ), new Excursion(new BigDecimal("75"), "Bicycle Tour",
                            "https://images.unsplash.com/uploads/14122621859313b34d52b/37e28531?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1473&q=80",
                            South_Dakota
                    ), new Excursion(new BigDecimal("250"), "Spa Treatment",
                            "https://images.unsplash.com/photo-1620733723572-11c53f73a416?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                            South_Dakota
                    ), new Excursion(new BigDecimal("100"), "Historic Tour",
                            "https://images.unsplash.com/photo-1479142506502-19b3a3b7ff33?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            South_Dakota
                    ), new Excursion(new BigDecimal("25"), "Boat Ride",
                            "https://images.unsplash.com/photo-1587252337395-d02401a8a814?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1026&q=80",
                            South_Dakota
                    ), new Excursion(new BigDecimal("500"), "Horseback Riding Lesson",
                            "https://images.unsplash.com/photo-1598810577851-34982c359155?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            South_Dakota
                    ), new Excursion(new BigDecimal("120"), "Zip Lining",
                            "https://images.unsplash.com/photo-1625076307714-a5cd1b2beb4f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            South_Dakota
                    ), new Excursion(new BigDecimal("150"), "Dinner and a Show",
                            "https://plus.unsplash.com/premium_photo-1661774645265-ce387923cb5b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            South_Dakota
                    ), new Excursion(new BigDecimal("100"), "Cheese Tour",
                            "https://images.unsplash.com/photo-1631379578550-7038263db699?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1474&q=80",
                            Nashville
                    ), new Excursion(new BigDecimal("75"), "Bicycle Tour",
                            "https://images.unsplash.com/uploads/14122621859313b34d52b/37e28531?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1473&q=80",
                            Nashville
                    ), new Excursion(new BigDecimal("250"), "Spa Treatment",
                            "https://images.unsplash.com/photo-1620733723572-11c53f73a416?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                            Nashville
                    ), new Excursion(new BigDecimal("100"), "Historic Tour",
                            "https://images.unsplash.com/photo-1479142506502-19b3a3b7ff33?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Nashville
                    ), new Excursion(new BigDecimal("25"), "Boat Ride",
                            "https://images.unsplash.com/photo-1587252337395-d02401a8a814?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1026&q=80",
                            Nashville
                    ), new Excursion(new BigDecimal("500"), "Horseback Riding Lesson",
                            "https://images.unsplash.com/photo-1598810577851-34982c359155?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Nashville
                    ), new Excursion(new BigDecimal("120"), "Zip Lining",
                            "https://images.unsplash.com/photo-1625076307714-a5cd1b2beb4f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Nashville
                    ), new Excursion(new BigDecimal("150"), "Dinner and a Show",
                            "https://plus.unsplash.com/premium_photo-1661774645265-ce387923cb5b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Nashville
                    ), new Excursion(new BigDecimal("100"), "Cheese Tour",
                            "https://images.unsplash.com/photo-1631379578550-7038263db699?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1474&q=80",
                            Wisconsin
                    ), new Excursion(new BigDecimal("75"), "Bicycle Tour",
                            "https://images.unsplash.com/uploads/14122621859313b34d52b/37e28531?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1473&q=80",
                            Wisconsin
                    ), new Excursion(new BigDecimal("250"), "Spa Treatment",
                            "https://images.unsplash.com/photo-1620733723572-11c53f73a416?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
                            Wisconsin
                    ), new Excursion(new BigDecimal("100"), "Historic Tour",
                            "https://images.unsplash.com/photo-1479142506502-19b3a3b7ff33?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Wisconsin
                    ), new Excursion(new BigDecimal("25"), "Boat Ride",
                            "https://images.unsplash.com/photo-1587252337395-d02401a8a814?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1026&q=80",
                            Wisconsin
                    ), new Excursion(new BigDecimal("500"), "Horseback Riding Lesson",
                            "https://images.unsplash.com/photo-1598810577851-34982c359155?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Wisconsin
                    ), new Excursion(new BigDecimal("120"), "Zip Lining",
                            "https://images.unsplash.com/photo-1625076307714-a5cd1b2beb4f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Wisconsin
                    ), new Excursion(new BigDecimal("150"), "Dinner and a Show",
                            "https://plus.unsplash.com/premium_photo-1661774645265-ce387923cb5b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                            Wisconsin
                ))));
        }
    }
}
