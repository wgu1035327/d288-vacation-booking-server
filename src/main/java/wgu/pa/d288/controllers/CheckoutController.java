package wgu.pa.d288.controllers;

import org.springframework.web.bind.annotation.*;
import wgu.pa.d288.services.CheckoutService;
import wgu.pa.d288.services.Purchase;
import wgu.pa.d288.services.PurchaseResponse;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
@RestController
@RequestMapping("/api/checkout")
public class CheckoutController {

    private CheckoutService checkoutService;

    public CheckoutController(CheckoutService checkoutService) {
        this.checkoutService = checkoutService;
    }

    @PostMapping("/purchase")
    public PurchaseResponse placeOrder(@RequestBody Purchase purchase) {
        PurchaseResponse purchaseResponse = checkoutService.placeOrder(purchase);
        return purchaseResponse;
    }
}
