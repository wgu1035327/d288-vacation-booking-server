package wgu.pa.d288.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.CrossOrigin;
import wgu.pa.d288.entities.Division;
import wgu.pa.d288.entities.Vacation;

import java.util.Optional;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
public interface DivisionRepository extends JpaRepository<Division, Long> {
    @Query("SELECT d from Division d where d.division_name = ?1")
    Optional<Division> findByName(String division_title);
}
