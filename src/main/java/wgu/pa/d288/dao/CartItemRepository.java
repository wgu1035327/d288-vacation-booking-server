package wgu.pa.d288.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import wgu.pa.d288.entities.CartItem;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
public interface CartItemRepository extends JpaRepository<CartItem, Long> {
}
