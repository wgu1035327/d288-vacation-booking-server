package wgu.pa.d288.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import wgu.pa.d288.entities.Vacation;

import java.util.Optional;

@CrossOrigin("http://vacationbooking.s3-website.us-east-2.amazonaws.com")
@Repository
public interface VacationRepository extends JpaRepository<Vacation, Long> {

    @Query("SELECT v from Vacation v where v.vacation_title = ?1")
    Vacation getVacationByVacation_title(String vacation_title);
}
