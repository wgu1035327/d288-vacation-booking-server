package wgu.pa.d288.services;

import lombok.Data;

@Data
public class PurchaseResponse {

    private final String orderTrackingNumber;
}
