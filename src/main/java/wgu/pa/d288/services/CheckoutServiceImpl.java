package wgu.pa.d288.services;

import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;
import wgu.pa.d288.dao.CartRepository;
import wgu.pa.d288.entities.Cart;
import wgu.pa.d288.entities.CartItem;
import wgu.pa.d288.entities.StatusType;

import java.util.Set;
import java.util.UUID;

@Service
public class CheckoutServiceImpl implements CheckoutService {

    private CartRepository cartRepository;

    public CheckoutServiceImpl(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Override
    @Transactional
    public PurchaseResponse placeOrder(Purchase purchase) {

        // retrieve data for cart and cartItems
        Cart cart = purchase.getCart();
        Set<CartItem> cartItems = purchase.getCartItems();

        // Display an error message to the Order confirmation page
        if (cartItems.isEmpty()) {
            return new PurchaseResponse("CART IS EMPTY, PLEASE ADD ITEMS. NOTHING HAS BEEN SAVED TO DATABASE.");
        }

        // associate cart with cartItems
        cartItems.forEach(item -> cart.add(item));

        // generate order tracking number and set cart.orderTrackingNumber
        String orderTrackingNumber = generateOrderTrackingNumber();
        cart.setOrderTrackingNumber(orderTrackingNumber);

        // set cart.status to "ordered"
        cart.setStatus(StatusType.ordered);

        // save cart to the database
        cartRepository.save(cart);

        return new PurchaseResponse(orderTrackingNumber);
    }

    private String generateOrderTrackingNumber() {
        return UUID.randomUUID().toString();
    }
}
