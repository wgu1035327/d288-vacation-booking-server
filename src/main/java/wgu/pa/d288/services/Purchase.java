package wgu.pa.d288.services;

import lombok.Data;
import wgu.pa.d288.entities.Cart;
import wgu.pa.d288.entities.CartItem;

import java.util.Set;

@Data
public class Purchase {

    private Cart cart;
    private Set<CartItem> cartItems;

}
