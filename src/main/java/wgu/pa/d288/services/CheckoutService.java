package wgu.pa.d288.services;

public interface CheckoutService {

    PurchaseResponse placeOrder(Purchase purchase);

}
