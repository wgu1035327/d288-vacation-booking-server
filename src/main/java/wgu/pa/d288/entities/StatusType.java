package wgu.pa.d288.entities;

public enum StatusType {
    pending,
    ordered,
    canceled
}
