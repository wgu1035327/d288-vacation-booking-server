FROM openjdk:17-jdk-slim

COPY target/d288-0.0.1-SNAPSHOT.jar /d288_vacation_booking.war

EXPOSE 5000

CMD ["java", "-jar", "/d288_vacation_booking.war"]
